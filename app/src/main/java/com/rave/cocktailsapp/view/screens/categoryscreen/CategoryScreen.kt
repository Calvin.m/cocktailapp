package com.rave.cocktailsapp.view.screens.categoryscreen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategory
import com.rave.cocktailsapp.view.navigation.Screens

@Composable
fun CategoryScreen(
    state: CategoryScreenState,
    navigate: (screen: Screens, category: CocktailCategory) -> Unit
) {
    LazyColumn() {
        items(state.categories) { category ->
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.clickable {
                    navigate(Screens.DrinksInCategoryScreen, category)
                }
            ) {
                Text(text = category.strCategory)
            }
        }
    }
}