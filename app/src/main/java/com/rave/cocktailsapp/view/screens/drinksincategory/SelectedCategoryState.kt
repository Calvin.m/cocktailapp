package com.rave.cocktailsapp.view.screens.drinksincategory

import com.rave.cocktailsapp.model.local.cocktaildrinksincategory.CocktailDrinksInCategory

data class SelectedCategoryState(
    val isLoading: Boolean = false,
//    val selectedCategory: List<CocktailDrinksInCategory> = CocktailDrinksInCategory(),
    val selectedCategoryDrinks: List<CocktailDrinksInCategory> = emptyList(),
    // val drinks:CocktailDrinksInCategory = List<CocktailDrinksInCategory>() = emptyList(),
    val errMsg: String = "error message in selected category state"
)