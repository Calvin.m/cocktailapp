package com.rave.cocktailsapp.view.screens.drinksincategory

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategory
import com.rave.cocktailsapp.view.navigation.Screens

@Composable
fun DrinksInCategoryScreen(
    state: SelectedCategoryState,
    navigate: (screen: Screens, drink: String) -> Unit
) {
    LazyColumn(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,

        ) {
        items(state.selectedCategoryDrinks) { drink ->
            Column(modifier = Modifier.clickable {
                navigate(Screens.DrinksInCategoryScreen, drink.strDrink)
            }) {
                Text(text = drink.strDrink)
            }
        }
    }
}

/*
@@Composable
fun CategoryMealScreen(state: CategoryMealState) {
    LazyColumn(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        items(state.categoryMeals) { meal ->
            Text(text = "Why you no make me a ${meal.strMeal}??????")
        }
    }
}
 */