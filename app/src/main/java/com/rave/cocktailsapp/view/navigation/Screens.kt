package com.rave.cocktailsapp.view.navigation

enum class Screens(val route: String) {
    CategoryListScreen("category_list"),
    DrinksInCategoryScreen("category_drinks"),
    SelectedDrinkDetailsScreen("drink_details")
}