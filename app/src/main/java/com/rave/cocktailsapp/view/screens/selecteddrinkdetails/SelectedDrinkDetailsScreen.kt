package com.rave.cocktailsapp.view.screens.selecteddrinkdetails

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.rave.cocktailsapp.view.navigation.Screens

@Composable
fun SelectedDrinkDetailsScreen(
    state: SelectedDrinkState,
    navigate: (screen: Screens, drink: String) -> Unit
) {
    if (state.selectedDrinkDetails.size < 1) {
        Text(text = "Loading....")
    } else {
        Column() {
            Text(text = "${state.selectedDrinkDetails[0].strDrink}")
        }
    }
}
