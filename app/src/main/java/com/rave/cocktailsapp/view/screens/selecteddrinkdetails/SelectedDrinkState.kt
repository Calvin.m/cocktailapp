package com.rave.cocktailsapp.view.screens.selecteddrinkdetails

import com.rave.cocktailsapp.model.local.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetails

data class SelectedDrinkState(
    val isLoading: Boolean = false,
    val selectedDrinkDetails: List<CocktailSelectedDrinkDetails> = emptyList(),
    val errMsg: String = "error in selectedDrinkState"

)

/*
data class SelectedCategoryState(
    val isLoading: Boolean = false,
//    val selectedCategory: List<CocktailDrinksInCategory> = CocktailDrinksInCategory(),
    val selectedCategoryDrinks: List<CocktailDrinksInCategory> = emptyList(),
    // val drinks:CocktailDrinksInCategory = List<CocktailDrinksInCategory>() = emptyList(),
    val errMsg: String = "error message in selected category state"
)
 */