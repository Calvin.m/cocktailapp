package com.rave.cocktailsapp.view.screens.categoryscreen

import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategoriesResponse
import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategory

data class CategoryScreenState(
    val isLoading: Boolean = false,
    val categories: List<CocktailCategory> = emptyList(),
    val error: String = "CategoryScreenState error"
)
