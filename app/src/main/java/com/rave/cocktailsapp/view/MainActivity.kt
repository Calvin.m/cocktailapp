package com.rave.cocktailsapp.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.rave.cocktailsapp.model.CocktailRepo
import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategory
import com.rave.cocktailsapp.model.local.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetails
import com.rave.cocktailsapp.model.mapper.CocktailCategoriesResponseMapper
import com.rave.cocktailsapp.model.mapper.CocktailDrinksInCategoryMapper
import com.rave.cocktailsapp.model.mapper.CocktailSelectedDrinkDetailsMapper
import com.rave.cocktailsapp.model.remote.CocktailRetrofit
import com.rave.cocktailsapp.ui.theme.CocktailsAppTheme
import com.rave.cocktailsapp.view.navigation.Screens
import com.rave.cocktailsapp.view.screens.categoryscreen.CategoryScreen
import com.rave.cocktailsapp.view.screens.drinksincategory.DrinksInCategoryScreen
import com.rave.cocktailsapp.view.screens.selecteddrinkdetails.SelectedDrinkDetailsScreen
import com.rave.cocktailsapp.viewmodel.CocktailVMFactory
import com.rave.cocktailsapp.viewmodel.CocktailViewModel

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : ComponentActivity() {
    private val viewModel: CocktailViewModel by viewModels {
        val repo = CocktailRepo(
            CocktailCategoriesResponseMapper(),
            CocktailDrinksInCategoryMapper(),
            CocktailSelectedDrinkDetailsMapper(),
            CocktailRetrofit().getCocktailService()
        )
        CocktailVMFactory(repo)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getCategories()
        setContent {
            CocktailsAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Color(86, 193, 241, 255)
                ) {
                    CocktailApp(
                        onCategorySelected = { selectedCategory: CocktailCategory ->
                            viewModel.getDrinksInCategory(selectedCategory)
                        },
                        onDrinkSelected = { selectedDrink: String ->
                            viewModel.getDrinkDetails(selectedDrink)
                        }
                    )
                }
            }
        }
    }
}

@Composable
fun CocktailApp(
    cocktailViewModel: CocktailViewModel = viewModel(),
    onCategorySelected: (category: CocktailCategory) -> Unit,
    onDrinkSelected: (drink: String) -> Unit
) {
    val state by cocktailViewModel.categories.collectAsState()
    val selectedCategory by cocktailViewModel.selectedCategoryDrinks.collectAsState()
    val selectedDrink by cocktailViewModel.selectedDrinkDetails.collectAsState()
    val controller = rememberNavController()

    NavHost(
        navController = controller,
        startDestination = Screens.CategoryListScreen.route
    ) {
        composable(Screens.CategoryListScreen.route) {
            CategoryScreen(state) { screen: Screens, category: CocktailCategory ->
                onCategorySelected(category)
                controller.navigate(screen.route)
            }
        }
        composable(Screens.DrinksInCategoryScreen.route) {
            DrinksInCategoryScreen(selectedCategory) { screen: Screens, drink: String ->
                onDrinkSelected(drink)
                println("()()(())()()()()")
                println(drink)
                println(screen.route)
//                controller.navigate(screen.route)
                controller.navigate(Screens.SelectedDrinkDetailsScreen.route)
            }
        }
        composable(Screens.SelectedDrinkDetailsScreen.route) {
            //todo - do this in viewmodel
            SelectedDrinkDetailsScreen(selectedDrink) { screen: Screens, drink: String ->
                println("#############")
                println(drink)
            }
        }
    }
}
