package com.rave.cocktailsapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rave.cocktailsapp.model.CocktailRepo

class CocktailVMFactory(
    val repo: CocktailRepo
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CocktailViewModel(repo) as T
    }
}