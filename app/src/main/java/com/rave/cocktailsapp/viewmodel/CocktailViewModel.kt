package com.rave.cocktailsapp.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.cocktailsapp.model.CocktailRepo
import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategory
import com.rave.cocktailsapp.model.local.cocktaildrinksincategory.CocktailDrinksInCategory
import com.rave.cocktailsapp.model.remote.NetworkResponse
import com.rave.cocktailsapp.view.screens.categoryscreen.CategoryScreenState
import com.rave.cocktailsapp.view.screens.drinksincategory.SelectedCategoryState
import com.rave.cocktailsapp.view.screens.selecteddrinkdetails.SelectedDrinkState
//import com.rave.cocktailsapp.view.screens.selecteddrinkdetails.SelectedDrinkState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class CocktailViewModel(
    private val repo: CocktailRepo
) : ViewModel() {
    private val _categories: MutableStateFlow<CategoryScreenState> =
        MutableStateFlow(CategoryScreenState())
    val categories: StateFlow<CategoryScreenState> get() = _categories

    private val _selectedCategoryDrinks: MutableStateFlow<SelectedCategoryState> =
        MutableStateFlow(SelectedCategoryState())
    val selectedCategoryDrinks: StateFlow<SelectedCategoryState> get() = _selectedCategoryDrinks

    private val _selectedDrinkDetails: MutableStateFlow<SelectedDrinkState> =
        MutableStateFlow(SelectedDrinkState())
        val selectedDrinkDetails: StateFlow<SelectedDrinkState> get() = _selectedDrinkDetails

    fun getCategories() = viewModelScope.launch {
        _categories.update {
            it.copy(isLoading = true)
        }
        val categoriesResponse = repo.getCocktailCategories()
        handleResults(categoriesResponse)

    }

    fun getDrinksInCategory(category: CocktailCategory) = viewModelScope.launch {
        _selectedCategoryDrinks.update { it.copy(isLoading = true) }
        val drinksInCategory = repo.getDrinksInCategory(category)
        handleResults(drinksInCategory)
    }

    fun getDrinkDetails(drinkName: String) = viewModelScope.launch {
        _selectedDrinkDetails.update { it.copy(isLoading = true) }
//        val drinkDetails = repo.getDrinkDetails(drink.st)
        val drinkDetails = repo.getDrinkDetails(drinkName)
        handleResults(drinkDetails)
    }

    private fun handleResults(result: NetworkResponse<*>) {
        when (result) {
            is NetworkResponse.Error -> _categories.update {
                it.copy(
                    isLoading = false,
                    error = result.msg
                )
            }
            NetworkResponse.Idle -> _categories.update {
                it.copy(isLoading = false)
            }
            NetworkResponse.Loading -> _categories.update {
                it.copy(isLoading = true)
            }
            is NetworkResponse.Success.CocktailCategorySuccess -> _categories.update {
                it.copy(isLoading = false, result.cocktailCategoryResponse.drinks)
            }

            is NetworkResponse.Success.CocktailDrinksInCategorySuccess -> _selectedCategoryDrinks.update {
                it.copy(isLoading = false, result.cocktailDrinksInCategoryResponse)
            }

            is NetworkResponse.Success.CocktailSelectedDrinkDetailsSuccess -> _selectedDrinkDetails.update {
//                it.copy(isLoading = false, result.cocktailSelectedDrinkDetailsResponse.drinks)
                it.copy(isLoading = false, result.cocktailSelectedDrinkDetailsResponse)
            }

            else -> {
                Log.e("CocktailViewModel", "error!")
                _categories.update { it.copy(isLoading = false, error = "CocktailViewModel error") }
            }
//            is NetworkResponse.Success.CocktailSelectedDrinkDetailsSuccess -> TODO()
        }
    }

}