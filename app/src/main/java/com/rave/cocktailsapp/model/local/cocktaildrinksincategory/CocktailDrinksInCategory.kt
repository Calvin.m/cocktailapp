package com.rave.cocktailsapp.model.local.cocktaildrinksincategory

data class CocktailDrinksInCategory(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)