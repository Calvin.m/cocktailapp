package com.rave.cocktailsapp.model.remote.dto.cocktailselecteddrinkdetails

import kotlinx.serialization.Serializable

@Serializable
data class CocktailSelectedDrinkDetailsResponseDTO(
    val drinks: List<CocktailSelectedDrinkDetailsDTO> = emptyList()
)