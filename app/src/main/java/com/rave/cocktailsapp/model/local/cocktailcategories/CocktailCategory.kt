package com.rave.cocktailsapp.model.local.cocktailcategories

data class CocktailCategory(
    val strCategory: String = ""
)