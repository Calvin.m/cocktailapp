package com.rave.cocktailsapp.model.local.cocktailcategories

data class CocktailCategoriesResponse(
    val drinks: List<CocktailCategory> = emptyList()
)