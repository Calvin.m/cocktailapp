package com.rave.cocktailsapp.model.mapper

import com.rave.cocktailsapp.model.local.cocktaildrinksincategory.CocktailDrinksInCategory
import com.rave.cocktailsapp.model.remote.dto.cocktaildrinksincategory.CocktailDrinksInCategoryDTO

class CocktailDrinksInCategoryMapper :
    Mapper<CocktailDrinksInCategoryDTO, CocktailDrinksInCategory> {
    override fun invoke(dto: CocktailDrinksInCategoryDTO): CocktailDrinksInCategory = with(dto) {
        CocktailDrinksInCategory(
            idDrink,
            strDrink,
            strDrinkThumb
        )
    }
}