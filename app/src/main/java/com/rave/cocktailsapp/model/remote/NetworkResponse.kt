package com.rave.cocktailsapp.model.remote

import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategoriesResponse
import com.rave.cocktailsapp.model.local.cocktaildrinksincategory.CocktailDrinksInCategory
import com.rave.cocktailsapp.model.local.cocktaildrinksincategory.CocktailDrinksInCategoryResponse
import com.rave.cocktailsapp.model.local.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetails
import com.rave.cocktailsapp.model.local.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetailsResponse

sealed class NetworkResponse<T> {
    object Idle : NetworkResponse<Unit>()
    object Loading : NetworkResponse<Unit>()
    data class Error(val msg: String) : NetworkResponse<String>()
    sealed class Success<T>() : NetworkResponse<T>() {
        data class CocktailCategorySuccess(val cocktailCategoryResponse: CocktailCategoriesResponse) :
            Success<CocktailCategoriesResponse>()

        data class CocktailDrinksInCategorySuccess(val cocktailDrinksInCategoryResponse:
                                                   List<CocktailDrinksInCategory>) :
            Success<CocktailDrinksInCategoryResponse>()

//        data class CocktailSelectedDrinkDetailsSuccess(val cocktailSelectedDrinkDetailsResponse: CocktailSelectedDrinkDetailsResponse) :
        data class CocktailSelectedDrinkDetailsSuccess(val cocktailSelectedDrinkDetailsResponse: List<CocktailSelectedDrinkDetails>) :
//            Success<CocktailSelectedDrinkDetailsResponse>()
            Success<List<CocktailSelectedDrinkDetails>>()
    }
}
