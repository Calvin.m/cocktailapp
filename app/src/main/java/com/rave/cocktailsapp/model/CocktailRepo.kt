package com.rave.cocktailsapp.model

import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategoriesResponse
import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategory
import com.rave.cocktailsapp.model.local.cocktaildrinksincategory.CocktailDrinksInCategory
import com.rave.cocktailsapp.model.local.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetails
import com.rave.cocktailsapp.model.mapper.*
import com.rave.cocktailsapp.model.remote.ApiService
import com.rave.cocktailsapp.model.remote.NetworkResponse
import com.rave.cocktailsapp.model.remote.dto.cocktailcategories.CocktailCategoriesResponseDTO
import com.rave.cocktailsapp.model.remote.dto.cocktailcategories.CocktailCategoryDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CocktailRepo(
//    val mapper: CocktailCategoriesMapper,
    val categoriesResponseMapper: CocktailCategoriesResponseMapper,
//    val drinksInCategoryResponseMapper: CocktailDrinksInCategoryResponseMapper,
    val drinksInCategoryMapper: CocktailDrinksInCategoryMapper,
    val drinksDetailsMapper: CocktailSelectedDrinkDetailsMapper,
    val service: ApiService
) {
    suspend fun getCocktailCategories(): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val res = service.getCocktailCategories().execute()
        return@withContext if (res.isSuccessful) {
            val resource:CocktailCategoriesResponseDTO = res.body() ?: CocktailCategoriesResponseDTO()
            //todo - error is some mismatch between here and NetworkResponse: Line 12
            //todo 1:05:25
            NetworkResponse.Success.CocktailCategorySuccess(categoriesResponseMapper(resource))
        } else {
            NetworkResponse.Error(res.message())
        }
    }

    suspend fun getDrinksInCategory(category: CocktailCategory): NetworkResponse<*> =
        withContext(Dispatchers.IO) {
            val categoryDrinksResponse = service.getCocktailDrinksInCategory(category.strCategory).execute()
            if (categoryDrinksResponse.isSuccessful) {
                val drinksInCategoryResponse = categoryDrinksResponse.body()?.drinks ?: emptyList()
//                val drinksInCategoryResponse = categoryDrinksResponse.body().drinks
                NetworkResponse.Success.CocktailDrinksInCategorySuccess(
                    //todo - suggestiong gets rid of big red line but then puts it under "it"
                    drinksInCategoryResponse.map { drinksInCategoryMapper(it) }
                )
            } else {
                NetworkResponse.Error(categoryDrinksResponse.message())
            }
        }

    suspend fun getDrinkDetails(drink: String): NetworkResponse<*> =
        withContext(Dispatchers.IO) {
            val drinkDetailsResponse = service.getCocktailSelectedDrinkDetails(drink).execute()
            if (drinkDetailsResponse.isSuccessful) {
                val drinksDets = drinkDetailsResponse.body()?.drinks ?: emptyList()
                NetworkResponse.Success.CocktailSelectedDrinkDetailsSuccess(
                    drinksDets.map { drinksDetailsMapper(it) }
                )
            } else {
                NetworkResponse.Error(drinkDetailsResponse.message())
            }
        }
}

/*

    suspend fun getMealInCategory(category: Category): NetworkResponse<*> =
        withContext(Dispatchers.IO) {
            val categoryMealResponse = service.getMealFromCategory(category.strCategory).execute()
            if (categoryMealResponse.isSuccessful) {
                val mealsInCategoryResponse =
                    categoryMealResponse.body()?.categoryMeals ?: emptyList()
                NetworkResponse.Success.CategoryMealSuccess(
                    mealsInCategoryResponse.map { categoryMealMapper(it) }
                )
            } else {
                NetworkResponse.Error(categoryMealResponse.message())
            }
        }
 */







/*
class RmRepo(
    val mapper: CharacterResponseMapper,
    val service: ApiService
) {
    suspend fun getCharacters(): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val res = service.getCharacters()
        if (res.isSuccessful) {
            val resource: CharacterResponseDTO = res.body() ?: CharacterResponseDTO()
            NetworkResponse.Success.CharacterSuccess(mapper(resource))

        } else {
            NetworkResponse.Error(res.message())
        }
    }
}
 */