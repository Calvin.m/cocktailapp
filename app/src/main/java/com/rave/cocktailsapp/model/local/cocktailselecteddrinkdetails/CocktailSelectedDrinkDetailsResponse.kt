package com.rave.cocktailsapp.model.local.cocktailselecteddrinkdetails

data class CocktailSelectedDrinkDetailsResponse(
    val drinks: List<CocktailSelectedDrinkDetails>
)