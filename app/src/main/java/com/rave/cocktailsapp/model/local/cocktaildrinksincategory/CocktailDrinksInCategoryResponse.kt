package com.rave.cocktailsapp.model.local.cocktaildrinksincategory

data class CocktailDrinksInCategoryResponse(
    val drinks: List<CocktailDrinksInCategory>
)