package com.rave.cocktailsapp.model.mapper

import com.rave.cocktailsapp.model.local.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetailsResponse
import com.rave.cocktailsapp.model.remote.dto.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetailsResponseDTO

class CocktailSelectedDrinkDetailsResponseMapper :
    Mapper<CocktailSelectedDrinkDetailsResponseDTO, CocktailSelectedDrinkDetailsResponse> {

    val cocktailSelectedDrinkDetailsMapper by lazy {
        CocktailSelectedDrinkDetailsMapper()
    }

    override fun invoke(dto: CocktailSelectedDrinkDetailsResponseDTO): CocktailSelectedDrinkDetailsResponse =
        with(dto) {
            CocktailSelectedDrinkDetailsResponse(drinks.map { cocktailSelectedDrinkDetailsMapper(it) })
        }
}