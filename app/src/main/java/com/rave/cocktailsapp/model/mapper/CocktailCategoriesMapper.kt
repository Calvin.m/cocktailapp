package com.rave.cocktailsapp.model.mapper

import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategory
import com.rave.cocktailsapp.model.remote.dto.cocktailcategories.CocktailCategoryDTO

class CocktailCategoriesMapper : Mapper<CocktailCategoryDTO, CocktailCategory> {
    override fun invoke(dto: CocktailCategoryDTO): CocktailCategory = with(dto) {
        CocktailCategory(
            strCategory
        )
    }
}