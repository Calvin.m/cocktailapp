package com.rave.cocktailsapp.model.remote.dto.cocktaildrinksincategory

import kotlinx.serialization.Serializable

@Serializable
data class CocktailDrinksInCategoryResponseDTO(
    val drinks: List<CocktailDrinksInCategoryDTO> = emptyList()
)