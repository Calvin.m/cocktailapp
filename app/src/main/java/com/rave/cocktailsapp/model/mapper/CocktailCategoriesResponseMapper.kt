package com.rave.cocktailsapp.model.mapper

import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategoriesResponse
import com.rave.cocktailsapp.model.remote.dto.cocktailcategories.CocktailCategoriesResponseDTO

class CocktailCategoriesResponseMapper :
    Mapper<CocktailCategoriesResponseDTO, CocktailCategoriesResponse> {

    val cocktailCategoryMapper by lazy {
        CocktailCategoriesMapper()
    }

    override fun invoke(dto: CocktailCategoriesResponseDTO): CocktailCategoriesResponse =
        with(dto) {
            CocktailCategoriesResponse(
                drinks.map { cocktailCategoryMapper(it) }
            )
        }
}