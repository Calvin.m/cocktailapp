package com.rave.cocktailsapp.model.mapper

import com.rave.cocktailsapp.model.local.cocktaildrinksincategory.CocktailDrinksInCategoryResponse
import com.rave.cocktailsapp.model.remote.dto.cocktaildrinksincategory.CocktailDrinksInCategoryResponseDTO

class CocktailDrinksInCategoryResponseMapper:
    Mapper<CocktailDrinksInCategoryResponseDTO, CocktailDrinksInCategoryResponse> {

    val cocktailDrinksInCategoryMapper by lazy {
        CocktailDrinksInCategoryMapper()
    }

    override fun invoke(dto: CocktailDrinksInCategoryResponseDTO): CocktailDrinksInCategoryResponse =
        with(dto) {
            CocktailDrinksInCategoryResponse(
                drinks.map { cocktailDrinksInCategoryMapper(it) }
            )
        }
}
