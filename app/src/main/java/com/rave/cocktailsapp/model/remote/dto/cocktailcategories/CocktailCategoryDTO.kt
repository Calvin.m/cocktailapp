package com.rave.cocktailsapp.model.remote.dto.cocktailcategories

import kotlinx.serialization.Serializable

@Serializable
data class CocktailCategoryDTO(
    val strCategory: String = ""
)