package com.rave.cocktailsapp.model.remote.dto.cocktaildrinksincategory

import kotlinx.serialization.Serializable

@Serializable
data class CocktailDrinksInCategoryDTO(
    val idDrink: String = "",
    val strDrink: String = "",
    val strDrinkThumb: String = ""
)