package com.rave.cocktailsapp.model.remote

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.create

@OptIn(ExperimentalSerializationApi::class)
class CocktailRetrofit {
    private val BASE_URL = "https://www.thecocktaildb.com/api/json/v1/1/"
    private val mediaType = "application/json".toMediaType()
    private val loggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    private var client: OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .build()
    private val converterFactory by lazy {
        Json {
            ignoreUnknownKeys = true
        }
            .asConverterFactory(mediaType)
    }

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
//        .addConverterFactory(Json.asConverterFactory(mediaType))
        .addConverterFactory(converterFactory)
        .build()

    fun getCocktailService(): ApiService = retrofit.create()
}

/*
@OptIn(ExperimentalSerializationApi::class)
class RmRetrofit {
    private val mediaType = "application/json".toMediaType()

    private val converterFactory by lazy {
        Json {
            ignoreUnknownKeys = true
        }
            .asConverterFactory(mediaType)
    }

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(ApiService.BASE_URL)
        .addConverterFactory(converterFactory)
        .build()

    fun getRMService(): ApiService = retrofit.create()
}
 */