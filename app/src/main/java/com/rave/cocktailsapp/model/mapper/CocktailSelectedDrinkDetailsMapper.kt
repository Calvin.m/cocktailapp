package com.rave.cocktailsapp.model.mapper

import com.rave.cocktailsapp.model.local.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetails
import com.rave.cocktailsapp.model.remote.dto.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetailsDTO

//todo set -> ?: "" for nullable vals
class CocktailSelectedDrinkDetailsMapper :
    Mapper<CocktailSelectedDrinkDetailsDTO, CocktailSelectedDrinkDetails> {
    override fun invoke(dto: CocktailSelectedDrinkDetailsDTO): CocktailSelectedDrinkDetails =
        with(dto) {
            CocktailSelectedDrinkDetails(
                dateModified,
                idDrink,
                strAlcoholic,
                strCategory,
                strCreativeCommonsConfirmed,
                strDrink,
                strDrinkAlternate ?: "",
                strDrinkThumb,
                strGlass,
                strIBA ?: "",
                strImageAttribution ?: "",
                strImageSource ?: "",
                strIngredient1,

//                strIngredient10,
//                strIngredient11,
//                strIngredient12,
//                strIngredient13,
//                strIngredient14,
//                strIngredient15,
//                strIngredient2,
//                strIngredient3,
//                strIngredient4,
//                strIngredient5,
//                strIngredient6,
//                strIngredient7,
//                strIngredient8,
//                strIngredient9,
//                strInstructions,

//                strInstructionsDE,
//                strInstructionsES,
//                strInstructionsFR,
//                strInstructionsIT,
//                strInstructionsZHHANS,
//                strInstructionsZHHANT,

//                strMeasure1,
//                strMeasure10,
//                strMeasure11,
//                strMeasure12,
//                strMeasure13,
//                strMeasure14,
//                strMeasure15,
//                strMeasure2,
//                strMeasure3,
//                strMeasure4,
//                strMeasure5,
//                strMeasure6,
//                strMeasure7,
//                strMeasure8,
//                strMeasure9,
//                strTags,
//                strVideo
            )
        }
}