package com.rave.cocktailsapp.model.remote

import com.rave.cocktailsapp.model.local.cocktailcategories.CocktailCategoriesResponse
import com.rave.cocktailsapp.model.local.cocktaildrinksincategory.CocktailDrinksInCategoryResponse
import com.rave.cocktailsapp.model.local.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetailsResponse
import com.rave.cocktailsapp.model.remote.dto.cocktailcategories.CocktailCategoriesResponseDTO
import com.rave.cocktailsapp.model.remote.dto.cocktaildrinksincategory.CocktailDrinksInCategoryResponseDTO
import com.rave.cocktailsapp.model.remote.dto.cocktailselecteddrinkdetails.CocktailSelectedDrinkDetailsResponseDTO
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    companion object {
        private const val COCKTAIL_CATEGORIES_ENDPOINT = "list.php"
        private const val COCKTAIL_DRINKS_IN_CATEGORY_ENDPOINT = "filter.php"
        private const val COCKTAIL_SELECTED_DRINK_DETAILS_ENDPOINT = "search.php"
    }

    @GET(COCKTAIL_CATEGORIES_ENDPOINT)
//    fun getCocktailCategories(@Query("c") categoryName: String): Call<CocktailCategoriesResponse>
    fun getCocktailCategories(@Query("c") categoryList: String = "list"):
            Call<CocktailCategoriesResponseDTO>

    @GET(COCKTAIL_DRINKS_IN_CATEGORY_ENDPOINT)
    fun getCocktailDrinksInCategory(@Query("c") categoryName: String):
            Call<CocktailDrinksInCategoryResponseDTO>

    @GET(COCKTAIL_SELECTED_DRINK_DETAILS_ENDPOINT)
    fun getCocktailSelectedDrinkDetails(@Query("s") drinkName: String):
            Call<CocktailSelectedDrinkDetailsResponseDTO>
}