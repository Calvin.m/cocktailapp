package com.rave.cocktailsapp.model.remote.dto.cocktailcategories

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
//import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlin.reflect.KClass

@Serializable
data class CocktailCategoriesResponseDTO(
    val drinks: List<CocktailCategoryDTO> = emptyList()
)